package ru.t1.fpavlov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by fpavlov on 06.02.2022.
 */
public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void init() {
        @NotNull final Iterable<AbstractCommand> commands = this.bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        this.es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (@NotNull final File file: this.folder.listFiles()) {
            if (file.isDirectory()) continue;
            @Nullable final String fileName = file.getName();
            final boolean check = this.commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    this.bootstrap.listenerCommand(fileName, false);
                } catch (@NotNull final Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stop() {
        this.es.shutdown();
    }

    public void start() {
        this.init();
    }

}
