package ru.t1.fpavlov.tm.api.service;

import ru.t1.fpavlov.tm.api.repository.IUserOwnedRepository;
import ru.t1.fpavlov.tm.model.AbstractUserOwnedModel;

/**
 * Created by fpavlov on 14.01.2022.
 */
public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

}
