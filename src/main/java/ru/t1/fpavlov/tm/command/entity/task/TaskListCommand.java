package ru.t1.fpavlov.tm.command.entity.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "List Task";

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final Sort sort = this.askEntitySort();
        @NotNull final String userId = this.getUserId();
        @NotNull final List<Task> tasks = this.getTaskService().findAll(userId, sort);
        this.renderEntities(tasks, "Tasks:");
    }

}
