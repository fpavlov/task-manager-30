package ru.t1.fpavlov.tm.exception.field;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class IdEmptyException extends AbstractFieldException {

    @NotNull
    public IdEmptyException() {
        super("Error! Id is empty");
    }

}
