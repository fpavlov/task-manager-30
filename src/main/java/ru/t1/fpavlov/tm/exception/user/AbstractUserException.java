package ru.t1.fpavlov.tm.exception.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.exception.AbstractException;

/**
 * Created by fpavlov on 20.12.2021.
 */
@NoArgsConstructor
public abstract class AbstractUserException extends AbstractException {

    @NotNull
    public AbstractUserException(@Nullable final String message) {
        super(message);
    }

    @NotNull
    public AbstractUserException(
            @Nullable final String message,
            @Nullable final Throwable cause
    ) {
        super(message, cause);
    }

    @NotNull
    public AbstractUserException(@Nullable final Throwable cause) {
        super(cause);
    }

    @NotNull
    public AbstractUserException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
