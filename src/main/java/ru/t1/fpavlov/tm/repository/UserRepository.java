package ru.t1.fpavlov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.repository.IUserRepository;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.exception.field.PasswordEmptyException;
import ru.t1.fpavlov.tm.exception.user.LoginEmptyException;
import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 20.12.2021.
 */
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User create(@Nullable final String login, @Nullable final String passwordHash) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordEmptyException();
        @NotNull User user = new User(login, passwordHash);
        return this.add(user);
    }

    @Nullable
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String passwordHash,
            @Nullable final String email
    ) {
        return this.add(new User(login, passwordHash, email));
    }

    @Nullable
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String passwordHash,
            @Nullable final Role role
    ) {
        return this.add(new User(login, passwordHash, role));
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        return this.findByTextField(User.LOGIN_FIELD_NAME, login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        return this.findByTextField(User.EMAIL_FIELD_NAME, email);
    }

    @Nullable
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        return this.findByTextField(User.LOGIN_FIELD_NAME, login) != null;
    }

    @Nullable
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        return this.findByTextField(User.EMAIL_FIELD_NAME, email) != null;
    }

}
